#import <Foundation/Foundation.h>
#import "objcpp.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        id inst = [[ObjCPP alloc] init];
        NSLog(@"Hello, World!");
    }
    return 0;
}
