#import <Foundation/Foundation.h>

#import "objcpp.h"
#import "cppclass.h"

struct CPPMembers {
    CPPClass member1;
};

@implementation ObjCPP

- (id)init
{
    self = [super init];
    if (self) {
        //Allocate storage for members
        _cppMembers = new CPPMembers;
        
        //usage
        NSLog(@"%d", _cppMembers->member1.getNumber());
    }
    
    return self;
}

- (void)dealloc
{
    //Free members even if ARC.
    delete _cppMembers;
    
    //If not ARC uncomment the following line
    //[super dealloc];
}

@end
