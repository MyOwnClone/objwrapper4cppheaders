#ifndef objwrapper4cppheaders_objcpp_h
#define objwrapper4cppheaders_objcpp_h

//Forward declare struct
struct CPPMembers;

@interface ObjCPP : NSObject
{
    //opaque pointer to store cpp members
    struct CPPMembers *_cppMembers;
}

@end

#endif
